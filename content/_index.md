+++
advanced-options = false
aliases = []
canonical = ""
description = ""
noindex = false
publishDate = 2021-06-15T05:00:00Z
seo_title = ""
slug = ""
title = "Home"
[[blocks]]
caption = ""
headline_1 = "We help companies serve their clients."
headline_2 = "Workflow, payments, and notifications"
image = "/uploads/screen-shot-2021-06-18-at-11-43-50-am.png"
overline = "Custom Client Portals"
sign_up = false
support_text = "<p>supportive text</p>"
template = "b-hero"
[[blocks]]
caption = ""
headline = "headline "
image = "/uploads/screen-shot-2021-06-18-at-11-43-50-am.png"
image_alignment = "right"
support_text = "<p>support text</p>"
template = "b-alternate-image"
[blocks.column_1]
headline = "first bullet"
support_text = "<p>stuff goes here</p>"
[[blocks.column_1.list]]
headline = "list item 2"
support_text = "<p>thing goes here</p>"
[[blocks.column_1.list]]
headline = "list item one"
support_text = "<p>thing here</p>"
[[blocks]]
headline = "Payments"
support_text = ""
template = "b-feature-grid"
[[blocks.feature_list]]
headline = "recurring payments"
support_text = "<ol><li><p>Balance paydown - </p></li><li><p></p></li></ol>"
[[blocks.feature_list]]
headline = "Memberships"
support_text = "<p></p>"
[[blocks.feature_list]]
headline = "Invoices"
support_text = "<p></p>"
[[blocks]]
headline = "Security features"
support_text = "<p>things about security here</p>"
template = "b-feature-grid"
[[blocks.feature_list]]
headline = ""
support_text = ""
[[blocks.feature_list]]
headline = "Multi-factor Authentication"
support_text = "<p>lorem ipsum, dolor sit amet. </p><p>lorem ipsum, dolor sit amet. </p><p>lorem ipsum, dolor sit amet. </p>"
[[blocks.feature_list]]
headline = "Encryption in transit and at rest"
support_text = "<p>support</p>"
[[blocks.feature_list]]
headline = "Continuous code security audit"
support_text = "<ul><li><p>checks Ruby on Rails applications for security vulnerabilities // <strong>A Ruby gem that offers patch-level verification for Bundler</strong> and helps you find security vulnerabilities in your Ruby dependencies. It checks for vulnerable versions of gems in Gemfile.lock and checks for insecure gem sources (http://).</p><p></p></li><li><p>Checks for vulnerable versions of gems in <code>Gemfile.lock</code>.</p></li><li><p>Checks for insecure gem sources (<code>http://</code>).</p></li><li><p>Allows ignoring certain advisories that have been manually worked around.</p></li><li><p>Static Analysis Security Tool</p></li></ul>"
[[blocks.feature_list]]
headline = "Database backups // cloud hosted"
support_text = "<p>support</p>"
[[blocks]]
headline = "headline for FAQs"
support_text = "<p>support for FAQs</p>"
template = "b-faqs"
[[blocks.faqs]]
headline = "question 1"
support_text = "<p>ansswer</p>"
[[blocks.faqs]]
headline = "question 2"
support_text = "<p>answer 2</p>"
[[blocks.faqs]]
headline = "question 3"
support_text = "<p>answer 3</p>"
[[blocks.faqs]]
headline = "question 4"
support_text = "<p>answer</p>"

+++
